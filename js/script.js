function excludeBy(mainArray, subArray, prop) {
  for (let mainItem of mainArray) {
    for (let subItem of subArray) {
      if (mainItem[prop] === subItem[prop]) {
        let idx = mainArray.indexOf(mainItem);
        mainArray.splice(idx, 1);
        break;
      }
    }
  }
  return mainArray;
}


let first = [
  {name:"Vlad", age:20},
  {name:"John", age:30},
  {name:"Ira", age:25},
  {name:"Anna", age:16}
];
let second = [
  {name:"Tom", age:20},
  {name:"Kim", age:75},
  {name:"Raph", age:16},
  {name:"Pip", age:10}
];
let p = "age";
console.log("Result ->",excludeBy(first, second, p));
